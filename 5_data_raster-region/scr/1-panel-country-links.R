### setup
cat("\f")
rm(list=ls())
source("packages.R")

### load data

### links
df = import('1_get_url/output/df_urls_files.rds')

### database
db = import('3_join_sf_to_tile/output/list_country_sf_tile_location.rds') %>% map(st_drop_geometry) %>% list_rbind()

### panel
panel=left_join(df,db,by=c('tile')) %>% 
      clean_names() %>% 
      relocate(gid_0,country,year,month,class,tile,id,url)

### export
export(panel,'5_data_raster-region/output/panel-links-country.rds')
