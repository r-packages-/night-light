### setup
cat("\f")
rm(list=ls())
source("packages.R")

### Load sf
db = import('3_join_sf_to_tile/output/list_country_sf_tile_location.rds')

### Iterating 
walk(.x = db,.f = function(y){
  
  ### Load Country
  country = y
  tile = country$tile
  country_name = country$GID_0 %>% unique()
  name = country$COUNTRY %>% unique()

  print(name)
  
  print('Stage 1. Done')
  
  ### Load Stars
  stars_path = list.files("4_download/output/tif/",full.names = T) %>% 
    str_subset('avg_rade9h') %>% str_subset(tile)
  
  stars = map(stars_path,.f = function(x) .f = read_stars(x,proxy = T))
  
  year = str_extract(stars_path,"(\\d{6})")
  
  print("Stage 2. Done")
  
  ### Cropped
  cropped  = map(.x = stars,.f = function(x){
    
    st_crop(x,y = country) %>% 
      st_as_sf(as_points=T) %>%
      tibble() %>% 
      tidyr::extract(geometry, c('lat', 'lon'), '\\((.*), (.*)\\)', convert = TRUE) 
    
    
  })
  
  print("Stage 3. Done")
  
  ### Set Names
  cropped=lapply(cropped,setNames,c("lights","lat","lon")) %>% 
    rbindlist(use.names = T,fill = T) %>% tibble()
  
  print("Stage 4. Done")
  
  ### Export
  export(cropped,paste0('5_crop/output/',country_name,'_',year,".rds"))
  
  print("Stage 5. Done")
  
})
