### setup
cat("\f")
rm(list=ls())
source("packages.R")

### Tar Files
tar_files = list.files(path = "4_download/output/tar/",full.names = T)

### Untaring
map(.x = tar_files,.f = function(x){
  
  ### Untar
  untar(x,exdir = '4_download/output/tif/')
  
  Sys.sleep(20)
  
})