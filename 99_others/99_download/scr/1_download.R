### setup
cat("\f")
rm(list=ls())
source("packages.R")

### urls
df = import("1_get_url/output/df_urls_files.rds") %>% 
  subset(year==2012 & month <=4)
urls = df %>% pull(url)

### Change File Path
file_path <- paste0(getwd(),"4_download_raster/lights/tar/") %>% str_replace_all("/", "\\\\\\\\")
fprof <- makeFirefoxProfile(list(browser.download.dir = file_path,
                                 browser.download.folderList = 2L,
                                 browser.download.manager.showWhenStarting = FALSE,
                                 browser.helperApps.neverAsk.openFile = "text/csv",
                                 browser.helperApps.neverAsk.saveToDisk = "text/csv"))

# Starting Server.
client_server <- RSelenium::rsDriver(browser=c("firefox"),
                                     geckover = "latest",
                                     chromever = NULL,
                                     port = netstat::free_ports() %>% sample(1),
                                     verbose = F,extraCapabilities=fprof)


# Creating client
rmDR = client_server$client

# close
rmDR$close()

### Iterating
map(.x = urls,.f = function(x){
  
  # open
  rmDR$open()
  
  # browse
  rmDR$navigate(x)
  
  # email
  email = rmDR$findElement('xpath','//*[@id="username"]')
  email$clickElement()
  email$sendKeysToElement(list("angelcastillo2060@gmail.com"))
  
  # password
  password = rmDR$findElement('xpath','//*[@id="password"]')
  password$clickElement()
  password$sendKeysToElement(list("Deisy1729"))
  
  # log in
  login = rmDR$findElement('xpath','//*[@id="kc-login"]')
  login$clickElement()
  
})


require(R.utils)
downloadFile(urls[[1]],path='4_download_raster/lights/tar/', username = "angelcastillo2060@gmail.com", password ="Deisy1729",verbose=T,binary=T)

