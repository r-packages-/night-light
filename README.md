





## 1_get_url

+ Obtener todas las url de los tiles a descargar:
  - VIIRS: https://eogdata.mines.edu/nighttime_light/monthly/v10/
  - DMSP: https://www.ngdc.noaa.gov/eog/dmsp/downloadV4composites.html
+ Output: Tibble que tenga lo siguiente:


id | año  | mes  | clase (vcmcfg o vcmslcfg)  | name_tile  |  url  |
--------------------------------------------------------------------
   |      |      |                            |            |       |  
--------------------------------------------------------------------
   |      |      |                            |            |       |  
--------------------------------------------------------------------

## 2_get_sf_data

+ Obtener todos los shapes para:
  - Pais
  - Region
  - Municipio
  
+ Output: 
  - Lista para los paises, en el nombre del elemento poner un ID unico.
  - Lista para las regiones, en el nombre del elemento poner un ID unico.
  - Lista para municipios, en el nombre del elemento poner un ID unico.

## 3_join_sf_to_tile

+ Obtener el tile que le corresponde a cada pais/region/municipio:
  1. Bajar los raster de los 6 tiles VIIRS para un mes.
  2. Para pais, te haces un st_join que te permita identificar los tiles a los que pertenece.
  3. Guardas en una base de datos.
+ Output: Tibble que tenga lo siguiente:

-----------------------
| id-pais | name_tile |
-----------------------
|         |           |
-----------------------
|         |           |     
-----------------------

## 4_downlad data
d_sf(region="COL",nivel="1",periodo="2010-2020")

+ 


## Productos

+ 1 Una fucnión que te devuelve los url de descarga de los tiles para una zona y periodo: VIIRS
+ 2 Una fucnión que te descarga los tiles para una zona y periodo: VIIRS
+ 3 Una función que te devuelve el shapefile con las grillas para una zona y periodo: VIIRS y Harmonized
+ 4 Una función que te devuelve un dataframe con unas decsriptivas (parecido a la funcion night calculate)

### Tareas 

### Para 1

Pegar el pais al tibble de 1 

### Para 2






