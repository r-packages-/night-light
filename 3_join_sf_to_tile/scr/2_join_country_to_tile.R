### setup
cat("\f")
rm(list=ls())
source("packages.R")

### Country
country = import("2_get_sf_data/output/gadm/list_sf_country_shape.rds")

### Rasters
layers = import("3_join_sf_to_tile/input/layers/layers_located.rds")

### Locate Each Country in each layer
f_locate = function(x){
  
  ### Quick transformation
  x = x %>% st_make_valid()
  
  ### Country
  location= st_join(x = x,y = layers)
  return(location)
}

### Mapping
location = map(.x = country,.f = f_locate,.progress = T)

### Names
names=map(location,function(x) dplyr::select(x,GID_0) %>% st_drop_geometry() %>% pull(GID_0) %>% .[[1]]) %>% as_vector()

### Set Names
names(location)=names

### Save 
export(location,"3_join_sf_to_tile/output/list_country_sf_tile_location.rds")
